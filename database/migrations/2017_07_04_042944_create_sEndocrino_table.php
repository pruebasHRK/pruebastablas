<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSEndocrinoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sEndocrino', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('poliuria');
            $table->boolean('polidipsia');
            $table->boolean('polifagia');
            $table->boolean('exoftalmos');
            $table->boolean('hipertension');
            $table->boolean('nerviosismo');
            $table->boolean('temblores');
            $table->boolean('insomio');
            $table->boolean('perdida_o_aumento_de_peso');
            $table->boolean('intolerancia_al_frio_o_calor');
            $table->longText('observaciones');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sEndocrino');
    }
}
