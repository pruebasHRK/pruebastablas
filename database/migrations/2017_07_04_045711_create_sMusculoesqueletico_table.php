<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSMusculoesqueleticoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sMusculoesqueletico', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('deformidad_articular');
            $table->boolean('dolor_articular');
            $table->boolean('limitacion_de_movimiento');
            $table->longText('observaciones');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sMusculoesqueletico');
    }
}
