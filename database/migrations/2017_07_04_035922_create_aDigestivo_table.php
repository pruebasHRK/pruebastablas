<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateADigestivoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aDigestivo', function (Blueprint $table) {
            $table->increments('id');
            
            $table->boolean('disfagia');
            $table->boolean('nauseas');
            $table->boolean('vomito');
            $table->boolean('diarrea');
            $table->boolean('pirosis');
            $table->boolean('ictericia');
            $table->longText('observaciones');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aDigestivo');
    }
}
