<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateARespiratorioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aRespiratorio', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('obstruccion_nasal');
            $table->boolean('tos');
            $table->boolean('rinorrea');
            $table->boolean('expectoracion');
            $table->boolean('disnea');
            $table->boolean('cianosis');
            $table->boolean('epistaxis');
            $table->boolean('hemoptisis');
            $table->longText('observaciones');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aRespiratorio');
    }
}
