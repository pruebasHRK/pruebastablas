<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePadecimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('padecimientos', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('description');
            $table->timestamps();
        });

        //Familiares y padecimientos = familiar_padecimiento
        Schema::create('familiar_padecimiento', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('familiar_id')->unsigned();
            $table->integer('padecimiento_id')->unsigned();

            $table->foreign('familiar_id')->references('id')->on('familiares');
            $table->foreign('padecimiento_id')->references('id')->on('padecimientos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('padecimientos');
    }
}
