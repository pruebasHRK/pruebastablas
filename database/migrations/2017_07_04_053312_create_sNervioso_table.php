<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSNerviosoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sNervioso', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('convulsiones');
            $table->boolean('cefalea');
            $table->boolean('lipotimia');
            $table->boolean('parestesia');
            $table->boolean('vertigo');
            $table->boolean('temblor');
            $table->longText('observaciones');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sNervioso');
    }
}
