<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateATegumentarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aTegumentario', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('cambio_de_color_en_piel');
            $table->boolean('erupciones');
            $table->boolean('prurito');
            $table->boolean('hiperhidrosis');
            $table->boolean('perdida_de_pelo_o_vello');
            $table->boolean('cutis_seco');
            $table->longText('observaciones');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aTegumentario');
    }
}
