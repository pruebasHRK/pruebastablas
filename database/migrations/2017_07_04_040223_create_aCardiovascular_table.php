<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateACardiovascularTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aCardiovascular', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('dolor_precordial');
            $table->boolean('fosfenos');
            $table->boolean('lipotimia');
            $table->boolean('taquicardia');
            $table->boolean('bradicardia');
            $table->boolean('hipertension');
            $table->boolean('acufenos');
            $table->boolean('cefalea');
            $table->boolean('mareos');
            $table->longText('observaciones');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aCardiovascular');
    }
}
