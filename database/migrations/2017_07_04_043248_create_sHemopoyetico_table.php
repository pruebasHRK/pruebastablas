<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSHemopoyeticoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sHemopoyetico', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('hemorragia');
            $table->boolean('epistaxis');
            $table->boolean('hematuria');
            $table->boolean('hematemesis');
            $table->boolean('petequias');
            $table->boolean('equimosis');
            $table->boolean('adenopatias');
            $table->longText('observaciones');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sHemopoyetico');
    }
}
