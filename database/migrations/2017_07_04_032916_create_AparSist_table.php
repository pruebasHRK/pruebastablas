<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAparSistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AparSist', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('digestivo_id')->unsigned();
            $table->integer('respiratorio_id')->unsigned();
            $table->integer('cardiovascular_id')->unsigned();
            $table->integer('genitourinario_id')->unsigned();
            $table->integer('endocrino_id')->unsigned();
            $table->integer('hemopoyetico_id')->unsigned();
            $table->integer('nervioso_id')->unsigned();
            $table->integer('musculoesqueletico_id')->unsigned();
            $table->integer('tegumentario_id')->unsigned();
            $table->integer('general_id')->unsigned();

            $table->integer('user_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('digestivo_id')->references('id')->on('aDigestivo');
            $table->foreign('respiratorio_id')->references('id')->on('aRespiratorio');
            $table->foreign('cardiovascular_id')->references('id')->on('aCardiovascular');
            $table->foreign('genitourinario_id')->references('id')->on('aGenitourinario');
            $table->foreign('endocrino_id')->references('id')->on('sEndocrino');
            $table->foreign('hemopoyetico_id')->references('id')->on('sHemopoyetico');

            $table->foreign('nervioso_id')->references('id')->on('sNervioso');
            $table->foreign('musculoesqueletico_id')->references('id')->on('sMusculoesqueletico');
            $table->foreign('tegumentario_id')->references('id')->on('aTegumentario');
            /*$table->foreign('general_id')->references('id')->on('users');*/

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AparSist');
    }
}
