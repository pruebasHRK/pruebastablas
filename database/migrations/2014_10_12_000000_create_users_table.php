<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');

            $table->integer('edad');
            $table->string('genero');
            $table->text('lugar_nacimiento');
            $table->date('fecha_nacimiento');
            $table->string('ocupacion');
            $table->string('escolaridad');
            $table->string('estado_civil');
            $table->text('calle');
            $table->text('numExterior');
            $table->text('numInterior');
            $table->text('colonia');
            $table->text('estado');
            $table->text('municipio');
            $table->text('delegacion');
            $table->text('telefono');
            $table->text('telefono_oficina');
            $table->text('medico_familiar');
            $table->longText('motivo_ultima_consulta');
            $table->text('telefonoMF');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
