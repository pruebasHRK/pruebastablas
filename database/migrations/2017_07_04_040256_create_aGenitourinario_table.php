<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAGenitourinarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aGenitourinario', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('incontinencia_urinaria');
            $table->boolean('dolor_lumbar');
            $table->boolean('disuria');
            $table->boolean('hematuria');
            $table->boolean('edema');
            $table->boolean('nicturia');
            $table->boolean('poliuiria');
            $table->longText('observaciones');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aGenitourinario');
    }
}
