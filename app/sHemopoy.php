<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sHemopoy extends Model
{
    protected $table = "sHemopoyetico";
	protected $fillable = ['hemorragia', 'epistaxis', 'hematuria', 'hematemesis', 'petequias', 'equimosis', 'adenopatias', 'observaciones'];//
}
