<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aDigest extends Model
{
    protected $table = "aDigestivo";
	protected $fillable = ['disfagia', 'nauseas', 'vomito', 'diarrea', 'pirosis', 'ictericia', 'observaciones'];//
}
