<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AprSis extends Model
{
    protected $table = "AparSist";
	protected $fillable = ['digestivo_id', 'respiratorio_id', 'cardiovascular_id', 'genitourinario_id', 'endocrino_id', 'hemopoyetico_id', 'nervioso_id', 'musculoesqueletico_id', 'tegumentario_id', 'general_id', 'user_id'
	];
}
