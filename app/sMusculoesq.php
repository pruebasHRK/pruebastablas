<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sMusculoesq extends Model
{
    protected $table = "sMusculoesqueletico";
	protected $fillable = ['deformidad_articular', 'dolor_articular', 'limitacion_de_movimiento', 'observaciones'];//
}
