<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sNerv extends Model
{
    protected $table = "sNervioso";
	protected $fillable = ['convulsiones', 'cefalea', 'lipotimia', 'parestesia', 'vertigo', 'temblor'];//
}
