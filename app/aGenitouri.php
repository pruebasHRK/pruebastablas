<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aGenitouri extends Model
{
    protected $table = "aGenitourinario";
	protected $fillable = ['incontinencia_urinaria', 'dolor_lumbar', 'disuria', 'hematuria', 'edema', 'nicturia', 'poliuiria', 'observaciones'
    ];
}
