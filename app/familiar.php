<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class familiar extends Model
{
	protected $table = "familiares";
	
    protected $fillable = ['parentesco', 'user_id'];
}
