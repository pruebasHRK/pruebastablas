<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aRespirat extends Model
{
    protected $table = "aRespiratorio";
	protected $fillable = ['obstrucción_nasal', 'tos', 'rinorrea', 'expectoracion', 'disnea', 'cianosis', 'epistaxis', 'hemoptisis', 'observaciones'];//
}
