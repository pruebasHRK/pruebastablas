<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sEndoc extends Model
{
    protected $table = "sEndocrino";
	protected $fillable = ['poliuria', 'polidipsia', 'polifagia', 'exoftalmos', 'hipertension', 'nerviosismo', 'temblores', 'insomio', 'perdida_o_aumento_de_peso', 'intolerancia_al_frio_o_calor', 'observaciones'];//
}
