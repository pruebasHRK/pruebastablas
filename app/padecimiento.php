<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class padecimiento extends Model
{
    protected $table = "padecimientos";
	protected $fillable = ['familiar_id', 'padecimiento_id'];//
}
