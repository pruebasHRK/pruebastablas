<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aCardiovasc extends Model
{
    protected $table = "aCardiovascular";
	protected $fillable = ['dolor_precordial', 'fosfenos', 'lipotimia', 'taquicardia', 'bradicardia', 'hipertension', 'acufenos', 'cefalea', 'mareos', 'observaciones'];//
}
