<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aTegument extends Model
{
    protected $table = "aTegumentario";
	protected $fillable = ['cambio_de_color_en_piel', 'erupciones', 'prurito', 'hiperhidrosis', 'perdida_de_pelo_o_vello', 'cutis_seco'];//
}
