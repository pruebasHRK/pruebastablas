<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'edad', 'genero', 'lugar_nacimiento', 'fecha_nacimiento', 'ocupacion', 'escolaridad', 'estado_civil', 'calle', 'numExterior', 'numInterior', 'colonia', 'estado', 'municipio', 'delegacion', 'telefono', 'telefono_oficina', 'medico_familiar', 'motivo_ultima_consulta', 'telefonoMF'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
